package ru.t1.akolobov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.dto.request.TaskListRequest;
import ru.t1.akolobov.tm.dto.response.TaskListResponse;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Display list of all tasks.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        TaskListRequest request = new TaskListRequest(getToken());
        request.setSortType(Sort.toSort(sortType));
        TaskListResponse response = getTaskEndpoint().listTask(request);
        @NotNull final List<TaskDto> taskList = response.getTaskList();
        renderTaskList(taskList);
    }

}
