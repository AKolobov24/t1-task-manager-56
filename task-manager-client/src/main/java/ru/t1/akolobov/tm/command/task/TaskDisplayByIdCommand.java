package ru.t1.akolobov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.akolobov.tm.dto.response.TaskGetByIdResponse;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.util.TerminalUtil;

@Component
public final class TaskDisplayByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-display-by-id";

    @NotNull
    public static final String DESCRIPTION = "Find task by Id and display.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setId(id);
        TaskGetByIdResponse response = getTaskEndpoint().getTaskById(request);
        @Nullable final TaskDto task = response.getTask();
        if (task == null) throw new TaskNotFoundException();
        displayTask(task);
    }

}
