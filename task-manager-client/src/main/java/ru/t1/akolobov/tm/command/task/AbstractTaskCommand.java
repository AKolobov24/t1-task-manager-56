package ru.t1.akolobov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.akolobov.tm.command.AbstractCommand;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;

import java.util.List;

@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @NotNull
    protected ITaskEndpoint getTaskEndpoint() {
        return this.taskEndpoint;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void renderTaskList(@NotNull final List<TaskDto> taskList) {
        int index = 1;
        for (@NotNull final TaskDto task : taskList) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void displayTask(@Nullable TaskDto task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
