package ru.t1.akolobov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.DataXmlLoadJaxBRequest;

@Component
public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        getDomainEndpoint().loadDataXmlJaxb(new DataXmlLoadJaxBRequest(getToken()));
    }

}
