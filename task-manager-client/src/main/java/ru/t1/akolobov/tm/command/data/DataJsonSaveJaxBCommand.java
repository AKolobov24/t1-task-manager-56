package ru.t1.akolobov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.DataJsonSaveJaxBRequest;

@Component
public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data to json file.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        getDomainEndpoint().saveDataJsonJaxb(new DataJsonSaveJaxBRequest(getToken()));
    }

}
