package ru.t1.akolobov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.UserRemoveRequest;
import ru.t1.akolobov.tm.util.TerminalUtil;

@Component
public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-remove";

    @NotNull
    public static final String DESCRIPTION = "User remove.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        UserRemoveRequest request = new UserRemoveRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().removeUser(request);
    }

}
