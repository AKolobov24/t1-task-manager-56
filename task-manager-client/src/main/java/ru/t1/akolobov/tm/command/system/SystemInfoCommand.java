package ru.t1.akolobov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.ApplicationSystemInfoRequest;
import ru.t1.akolobov.tm.dto.response.ApplicationSystemInfoResponse;
import ru.t1.akolobov.tm.util.FormatUtil;

@Component
public final class SystemInfoCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "info";

    @NotNull
    public static final String ARGUMENT = "-i";

    @NotNull
    public static final String DESCRIPTION = "Display system resources information.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        @NotNull ApplicationSystemInfoResponse response =
                getSystemEndpoint().getSystemInfo(new ApplicationSystemInfoRequest());
        System.out.println("Server host name: " + response.getHostName());
        System.out.println("Available processors: " + response.getProcessors());
        System.out.println("Free memory: " + FormatUtil.formatBytes(response.getFreeMemory()));
        @NotNull final String maxMemoryFormat = FormatUtil.formatBytes(response.getMaxMemory());
        @NotNull final String maxMemoryValue =
                (response.getMaxMemory() == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory available to JVM: " + FormatUtil.formatBytes(response.getTotalMemory()));
        final long usedMemory = response.getTotalMemory() - response.getFreeMemory();
        System.out.println("Used memory by JVM: " + FormatUtil.formatBytes(usedMemory));
    }

}
