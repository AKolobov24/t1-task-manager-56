package ru.t1.akolobov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.akolobov.tm.command.AbstractCommand;

@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return this.domainEndpoint;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
