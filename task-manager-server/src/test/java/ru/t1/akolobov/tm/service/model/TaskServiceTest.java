package ru.t1.akolobov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.api.repository.model.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.model.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.model.IUserRepository;
import ru.t1.akolobov.tm.api.service.model.ITaskService;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.data.model.TestProject;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.*;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.repository.model.ProjectRepository;
import ru.t1.akolobov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestTask.createTask;
import static ru.t1.akolobov.tm.data.model.TestTask.createTaskList;
import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    @Autowired
    private static IUserRepository userRepository;

    @NotNull
    private final static EntityManager repositoryEntityManager = userRepository.getEntityManager();

    @NotNull
    private final ITaskRepository repository = new TaskRepository(repositoryEntityManager);
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(repositoryEntityManager);
    @NotNull
    @Autowired
    private ITaskService service;

    @BeforeClass
    public static void addUsers() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.add(USER1);
        userRepository.add(USER2);
        repositoryEntityManager.getTransaction().commit();
    }

    @AfterClass
    public static void clearUsers() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        repositoryEntityManager.getTransaction().commit();
        repositoryEntityManager.close();
    }

    @Before
    public void initRepository() {
        repositoryEntityManager.getTransaction().begin();
        createTaskList(USER1).forEach(repository::add);
        repositoryEntityManager.getTransaction().commit();
    }

    @After
    public void clearRepository() {
        repositoryEntityManager.getTransaction().begin();
        repository.clear(USER1_ID);
        repository.clear(USER2_ID);
        projectRepository.clear(USER2_ID);
        repositoryEntityManager.getTransaction().commit();
    }

    @Test
    public void add() {
        @NotNull final Task task = createTask(USER1);
        service.add(USER1_ID, task);
        Assert.assertEquals(task, repository.findOneById(task.getUser().getId(), task.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(USER_EMPTY_ID, task));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(USER_EMPTY_ID));
        @NotNull final List<Task> taskList = createTaskList(USER2);
        service.add(taskList);
        Assert.assertFalse(service.findAll(USER2_ID).isEmpty());
        service.clear(USER2_ID);
        Assert.assertTrue(service.findAll(USER2_ID).isEmpty());
    }

    @Test
    public void existById() {
        @NotNull final Task task = createTask(USER1);
        service.add(task);
        Assert.assertTrue(service.existById(USER1_ID, task.getId()));
        Assert.assertFalse(service.existById(USER2_ID, task.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1_ID, USER_EMPTY_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(USER_EMPTY_ID, task.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> taskList = createTaskList(USER2);
        service.add(taskList);
        Assert.assertEquals(taskList, service.findAll(USER2_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(USER_EMPTY_ID));
    }


    @Test
    public void findAllSorted() {
        @NotNull final Task task = createTask(USER1);
        task.setName("task-0");
        task.setDescription("task-0-desc");
        @NotNull final List<Task> taskList = new ArrayList<>();
        taskList.add(task);
        taskList.addAll(service.findAll(USER1_ID));
        service.add(task);
        Assert.assertEquals(taskList, service.findAll(USER1_ID, Sort.BY_NAME));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findAll(USER_EMPTY_ID, Sort.BY_NAME)
        );
    }

    @Test
    public void findOneById() {
        @NotNull final Task task = createTask(USER1);
        service.add(task);
        Assert.assertEquals(task, service.findOneById(USER1_ID, task.getId()));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneById(USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.findOneById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void getSize() {
        int size = service.findAll(USER1_ID).size();
        Assert.assertEquals(size, service.getSize(USER1_ID).intValue());
        service.add(createTask(USER1));
        Assert.assertEquals(size + 1, service.getSize(USER1_ID).intValue());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(USER_EMPTY_ID));
    }

    @Test
    public void remove() {
        @NotNull final List<Task> taskList = service.findAll(USER1_ID);
        int size = taskList.size();
        @NotNull final Task task = taskList.get(0);
        Assert.assertNotNull(task);
        service.remove(USER1_ID, task);
        Assert.assertFalse(service.findAll(USER1_ID).contains(task));
        Assert.assertNull(service.findOneById(USER1_ID, task.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(USER_EMPTY_ID, task));
    }

    @Test
    public void removeById() {
        @NotNull final List<Task> taskList = service.findAll(USER1_ID);
        int size = taskList.size();
        @NotNull final Task task = taskList.get(0);
        Assert.assertNotNull(task);
        service.removeById(USER1_ID, task.getId());
        Assert.assertFalse(service.findAll(USER1_ID).contains(task));
        Assert.assertNull(service.findOneById(USER1_ID, task.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(USER_EMPTY_ID, task.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER1_ID, USER_EMPTY_ID));
    }

    @Test
    public void changeStatusById() {
        Task task = createTask(USER1);
        service.add(USER1_ID, task);
        Assert.assertNotNull(task);
        @NotNull final String newTaskId = task.getId();
        Assert.assertNotNull(service.changeStatusById(USER1_ID, newTaskId, Status.IN_PROGRESS));
        task = service.findOneById(USER1_ID, newTaskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusById(USER_EMPTY_ID, newTaskId, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeStatusById(USER1_ID, USER_EMPTY_ID, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusById(USER1_ID, newTaskId, null)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.changeStatusById(USER2_ID, newTaskId, Status.IN_PROGRESS)
        );
    }

    @Test
    public void create() {
        @NotNull final Task task = createTask(USER1);
        Task newTask = service.create(USER1_ID, task.getName());
        Assert.assertNotNull(newTask);
        newTask = service.findOneById(USER1_ID, newTask.getId());
        Assert.assertNotNull(newTask);
        Assert.assertEquals(task.getName(), newTask.getName());

        newTask = service.create(USER1_ID, task.getName(), task.getDescription());
        Assert.assertNotNull(newTask);
        newTask = service.findOneById(USER1_ID, newTask.getId());
        Assert.assertNotNull(newTask);
        Assert.assertEquals(task.getName(), newTask.getName());
        Assert.assertEquals(task.getDescription(), newTask.getDescription());

        task.setStatus(Status.IN_PROGRESS);
        newTask = service.create(USER1_ID, task.getName(), task.getStatus());
        Assert.assertNotNull(newTask);
        newTask = service.findOneById(USER1_ID, newTask.getId());
        Assert.assertNotNull(newTask);
        Assert.assertEquals(task.getName(), newTask.getName());
        Assert.assertEquals(task.getStatus(), newTask.getStatus());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.create(USER_EMPTY_ID, task.getName())
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.create(USER1_ID, "")
        );
    }

    @Test
    public void updateById() {
        @NotNull final Task task = service.findAll(USER1_ID).get(0);
        Assert.assertNotNull(task);
        @NotNull final String newName = "NewName";
        @NotNull final String newDescription = "NewDescription";
        service.updateById(USER1_ID, task.getId(), newName, newDescription);
        final Task newTask = service.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(newTask);
        Assert.assertEquals(newName, newTask.getName());
        Assert.assertEquals(newDescription, newTask.getDescription());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById(USER_EMPTY_ID, task.getId(), newName, newDescription)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(USER1_ID, USER_EMPTY_ID, newName, newDescription)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(USER1_ID, task.getId(), "", newDescription)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.updateById(USER2_ID, task.getId(), newName, newDescription)
        );
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final Project project = TestProject.createProject(USER2);
        repositoryEntityManager.getTransaction().begin();
        projectRepository.add(project);
        repositoryEntityManager.getTransaction().commit();
        @NotNull final List<Task> taskList = createTaskList(USER2);
        taskList.forEach((t) -> t.setProject(project));
        service.add(taskList);
        service.add(USER2_ID, createTask(USER2));
        Assert.assertEquals(taskList, service.findAllByProjectId(USER2_ID, project.getId()));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findAllByProjectId(USER_EMPTY_ID, project.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.findAllByProjectId(USER2_ID, USER_EMPTY_ID)
        );
    }

}
