package ru.t1.akolobov.tm.data.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class TestProject {

    @NotNull
    public static Project createProject() {
        return new Project("new-project", "new-project-desc");
    }

    @NotNull
    public static Project createProject(@NotNull final User user) {
        @NotNull Project project = createProject();
        project.setUser(user);
        return project;
    }

    @NotNull
    public static List<Project> createProjectList(@NotNull final User user) {
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            @NotNull Project project = new Project("project-" + i, "project-" + i + "desc");
            project.setUser(user);
            projectList.add(project);
        }
        return projectList;
    }

}
