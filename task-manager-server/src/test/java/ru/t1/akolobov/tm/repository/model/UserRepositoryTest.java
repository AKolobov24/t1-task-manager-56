package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    private static EntityManager entityManager;

    @BeforeClass
    public static void prepareConnection() {
        entityManager = context.getBean(EntityManager.class);
    }

    @AfterClass
    public static void closeConnection() {
        entityManager.close();
    }

    @Before
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @Test
    public void add() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.findAll().contains(USER1));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    @Ignore
    public void clear() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        @NotNull final List<User> userList = createUserList();
        userList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(userList.size(), repository.findAll().size());
        entityManager.getTransaction().begin();
        repository.clear();
        entityManager.getTransaction().commit();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void existById() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.existById(USER1.getId()));
        Assert.assertFalse(repository.existById(USER2.getId()));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findAll() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        @NotNull final List<User> user1UserList = createUserList();
        user1UserList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.findAll().containsAll(user1UserList));
        entityManager.getTransaction().begin();
        user1UserList.forEach(repository::remove);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findOneById() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
        Assert.assertNull(repository.findOneById(USER2.getId()));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    public void getSize() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        @NotNull final List<User> userList = repository.findAll();
        Assert.assertEquals(userList.size(), repository.getSize().intValue());
        repository.add(NEW_USER);
        entityManager.getTransaction().commit();
        Assert.assertEquals((userList.size() + 1), repository.getSize().intValue());
        entityManager.getTransaction().begin();
        repository.remove(NEW_USER);
        entityManager.getTransaction().commit();
    }

    @Test
    public void remove() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        repository.add(NEW_USER);
        entityManager.getTransaction().commit();
        Assert.assertEquals(NEW_USER, repository.findOneById(NEW_USER.getId()));
        @NotNull final Long size = repository.getSize();
        entityManager.getTransaction().begin();
        repository.remove(NEW_USER);
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(NEW_USER.getId()));
        Assert.assertEquals(size - 1, repository.getSize().intValue());
    }

    @Test
    public void removeById() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        repository.add(NEW_USER);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(repository.findOneById(NEW_USER.getId()));
        entityManager.getTransaction().begin();
        repository.removeById(NEW_USER.getId());
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(NEW_USER.getId()));
    }

    @Test
    public void findByLogin() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertEquals(USER1, repository.findByLogin(USER1.getLogin()));
        Assert.assertNull(repository.findOneById(USER2.getLogin()));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findByEmail() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertEquals(USER1, repository.findByEmail(USER1.getEmail()));
        Assert.assertNull(repository.findByEmail("user_2@email.ru"));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    public void isLoginExist() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(repository.isLoginExist(USER2.getLogin()));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    public void isEmailExist() {
        @NotNull final UserRepository repository = new UserRepository(entityManager);
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(repository.isEmailExist("user_2@email.ru"));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

}
