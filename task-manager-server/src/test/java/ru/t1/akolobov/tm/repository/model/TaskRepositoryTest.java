package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestTask.createTask;
import static ru.t1.akolobov.tm.data.model.TestTask.createTaskList;
import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    private static EntityManager entityManager;

    @BeforeClass
    public static void prepareConnection() {
        entityManager = context.getBean(EntityManager.class);
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.add(USER1);
        userRepository.add(USER2);
        entityManager.getTransaction().commit();
    }

    @AfterClass
    public static void closeConnection() {
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Before
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @After
    public void clearData() {
        @NotNull final TaskRepository repository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
        entityManager.getTransaction().commit();
    }

    @Test
    public void add() {
        @NotNull final TaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Task task = createTask(USER1);
        repository.add(task);
        entityManager.getTransaction().commit();
        Assert.assertEquals(task, repository.findAll(USER1_ID).get(0));
        Assert.assertEquals(1, repository.findAll(USER1_ID).size());
    }

    @Test
    public void clear() {
        @NotNull final TaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> TaskList = createTaskList(USER1);
        TaskList.forEach(repository::add);
        entityManager.getTransaction().commit();
        long size = repository.getSize();
        entityManager.getTransaction().begin();
        repository.clear(USER1_ID);
        entityManager.getTransaction().commit();
        Assert.assertEquals(
                size - TaskList.size(),
                repository.getSize().intValue()
        );
    }

    @Test
    public void existById() {
        @NotNull final TaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Task task = createTask(USER1);
        repository.add(task);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.existById(USER1_ID, task.getId()));
        Assert.assertFalse(repository.existById(USER2_ID, task.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final TaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> user1TaskList = createTaskList(USER1);
        @NotNull final List<Task> user2TaskList = createTaskList(USER2);
        user1TaskList.forEach(repository::add);
        user2TaskList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(user1TaskList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2TaskList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull final TaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Task task = createTask(USER1);
        repository.add(task);
        entityManager.getTransaction().commit();
        @NotNull final String taskId = task.getId();
        Assert.assertEquals(task, repository.findOneById(USER1_ID, taskId));
        Assert.assertNull(repository.findOneById(USER2_ID, taskId));
    }

    @Test
    public void getSize() {
        @NotNull TaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> taskList = createTaskList(USER1);
        taskList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(taskList.size(), repository.getSize(USER1_ID).intValue());
        entityManager.getTransaction().begin();
        repository.add(createTask(USER1));
        entityManager.getTransaction().commit();
        Assert.assertEquals((taskList.size() + 1), repository.getSize(USER1_ID).intValue());
    }

    @Test
    public void remove() {
        @NotNull final TaskRepository repository = new TaskRepository(entityManager);
        createTaskList(USER1).forEach(repository::add);
        @NotNull final Task task = createTask(USER1);
        repository.add(task);
        entityManager.getTransaction().commit();
        Assert.assertEquals(task, repository.findOneById(USER1_ID, task.getId()));
        entityManager.getTransaction().begin();
        repository.remove(task);
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
    }

    @Test
    public void removeById() {
        @NotNull final TaskRepository repository = new TaskRepository(entityManager);
        createTaskList(USER1).forEach(repository::add);
        @NotNull final Task task = createTask(USER1);
        repository.add(task);
        repository.removeById(USER1_ID, task.getId());
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final TaskRepository repository = new TaskRepository(entityManager);
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final Project project = new Project("test-project-for-task");
        project.setUser(USER1);
        projectRepository.add(project);
        entityManager.getTransaction().commit();
        @NotNull final String projectId = project.getId();
        @NotNull final List<Task> taskList = createTaskList(USER1);
        taskList.forEach(t -> t.setProject(project));
        entityManager.getTransaction().begin();
        taskList.forEach(repository::add);
        repository.add(createTask(USER1));
        entityManager.getTransaction().commit();
        Assert.assertEquals(taskList, repository.findAllByProjectId(USER1_ID, projectId));
        entityManager.getTransaction().begin();
        taskList.forEach(repository::remove);
        projectRepository.removeById(USER1_ID, projectId);
        entityManager.getTransaction().commit();
    }

}
