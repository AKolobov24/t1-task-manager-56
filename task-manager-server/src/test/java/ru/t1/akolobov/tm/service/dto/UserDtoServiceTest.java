package ru.t1.akolobov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.dto.IUserDtoService;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.user.*;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.dto.TestUserDto.*;

@Category(UnitCategory.class)
public final class UserDtoServiceTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    @Autowired
    private static IPropertyService propertyService;

    @NotNull
    @Autowired
    private static IUserDtoRepository userRepository;

    @NotNull
    private final static EntityManager repositoryEntityManager = userRepository.getEntityManager();

    @NotNull
    @Autowired
    private static IUserDtoService service;

    private static List<UserDto> userList;

    @BeforeClass
    public static void initRepository() {
        userList = createUserList();
        repositoryEntityManager.getTransaction().begin();
        userList.forEach(userRepository::add);
        repositoryEntityManager.getTransaction().commit();
    }

    @AfterClass
    public static void clearRepository() {
        repositoryEntityManager.getTransaction().begin();
        userList.forEach(user -> userRepository.removeById(user.getId()));
        repositoryEntityManager.getTransaction().commit();
        repositoryEntityManager.close();
    }

    @Test
    public void create() {
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.create(USER_EMPTY_ID, NEW_USER.getLogin(), Role.ADMIN)
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> service.create(NEW_USER.getLogin(), USER_EMPTY_ID, Role.ADMIN)
        );
        Assert.assertThrows(
                RoleEmptyException.class,
                () -> service.create(NEW_USER.getLogin(), NEW_USER.getLogin(), (Role) null)
        );

        service.create(NEW_USER.getLogin(), NEW_USER.getLogin(), Role.ADMIN);
        NEW_USER.setPasswordHash(HashUtil.salt(propertyService, NEW_USER.getLogin()));
        @Nullable final UserDto newUser = userRepository.findByLogin(NEW_USER.getLogin());
        Assert.assertNotNull(newUser);
        Assert.assertEquals(NEW_USER.getLogin(), newUser.getLogin());
        Assert.assertEquals(NEW_USER.getPasswordHash(), newUser.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, newUser.getRole());
        Assert.assertThrows(
                LoginExistException.class,
                () -> service.create(NEW_USER.getLogin(), NEW_USER.getLogin(), Role.ADMIN)
        );
        repositoryEntityManager.getTransaction().begin();
        userRepository.removeById(newUser.getId());
        repositoryEntityManager.getTransaction().commit();
    }

    @Test
    public void removeByLogin() {
        service.add(NEW_USER);
        final int size = service.findAll().size();
        Assert.assertNotNull(service.findByLogin(NEW_USER.getLogin()));
        service.removeByLogin(NEW_USER.getLogin());
        Assert.assertEquals(size - 1, service.findAll().size());
        Assert.assertNull(service.findByLogin(NEW_USER.getLogin()));

        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.removeByLogin(USER_EMPTY_ID)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.removeByLogin(NEW_USER.getLogin())
        );
    }

    @Test
    public void removeByEmail() {
        service.add(NEW_USER);
        final int size = service.findAll().size();
        Assert.assertNotNull(service.findByLogin(NEW_USER.getLogin()));
        service.removeByEmail(NEW_USER.getEmail());
        Assert.assertEquals(size - 1, service.findAll().size());
        Assert.assertNull(service.findByLogin(NEW_USER.getLogin()));

        Assert.assertThrows(
                EmailEmptyException.class,
                () -> service.removeByEmail(USER_EMPTY_ID)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.removeByEmail(NEW_USER.getEmail())
        );
    }

    @Test
    public void setPassword() {
        @NotNull final UserDto user = service.findAll().get(0);
        @NotNull final String passwordHash = user.getPasswordHash();
        service.setPassword(user.getId(), "new_password");
        @Nullable final UserDto updatedUser = service.findOneById(user.getId());
        Assert.assertNotNull(updatedUser);
        Assert.assertNotEquals(passwordHash, updatedUser.getPasswordHash());
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.setPassword(USER_EMPTY_ID, "new_password")
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> service.setPassword(user.getId(), "")
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.setPassword(USER2_ID, "new_password")
        );
    }

    @Test
    public void updateUser() {
        @NotNull final String firstName = "Firstname";
        @NotNull final String lastName = "LastName";
        @NotNull final String middleName = "MiddleName";
        @NotNull final UserDto user = userRepository.findAll().get(0);
        service.updateUser(user.getId(), firstName, lastName, middleName);
        @Nullable final UserDto updatedUser = userRepository.findOneById(user.getId());
        repositoryEntityManager.refresh(updatedUser);
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(updatedUser.getFirstName(), firstName);
        Assert.assertEquals(updatedUser.getLastName(), lastName);
        Assert.assertEquals(updatedUser.getMiddleName(), middleName);
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateUser(USER_EMPTY_ID, firstName, lastName, middleName)
        );
        Assert.assertThrows(
                FirstNameEmptyException.class,
                () -> service.updateUser(user.getId(), "", lastName, middleName)
        );
        Assert.assertThrows(
                LastNameEmptyException.class,
                () -> service.updateUser(user.getId(), firstName, "", middleName)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.updateUser(USER2_ID, firstName, lastName, middleName)
        );
    }

    @Test
    public void lockUserByLogin() {
        @Nullable final UserDto user = service.findOneById(userList.get(0).getId());
        Assert.assertNotNull(user);
        Assert.assertFalse(user.isLocked());
        service.lockUserByLogin(user.getLogin());
        @Nullable final UserDto lockedUser = service.findOneById(user.getId());
        Assert.assertNotNull(lockedUser);
        Assert.assertTrue(lockedUser.isLocked());
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.lockUserByLogin(USER_EMPTY_ID)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.lockUserByLogin(USER2_ID)
        );
    }

    @Test
    public void unlockUserByLogin() {
        UserDto user = userRepository.findAll().get(0);
        service.lockUserByLogin(user.getLogin());
        user = userRepository.findByLogin(user.getLogin());
        repositoryEntityManager.refresh(user);
        Assert.assertNotNull(user);
        Assert.assertTrue(user.isLocked());
        service.unlockUserByLogin(user.getLogin());
        @Nullable final UserDto unlockedUser = userRepository.findOneById(user.getId());
        repositoryEntityManager.refresh(unlockedUser);
        Assert.assertNotNull(unlockedUser);
        Assert.assertFalse(unlockedUser.isLocked());
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.unlockUserByLogin(USER_EMPTY_ID)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.unlockUserByLogin(USER2_ID)
        );
    }

    @Test
    public void findByLogin() {
        service.add(NEW_USER);
        @Nullable final UserDto user = service.findByLogin(NEW_USER.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(NEW_USER.getLogin(), user.getLogin());
        Assert.assertEquals(NEW_USER.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(NEW_USER.getRole(), user.getRole());
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.findByLogin(USER_EMPTY_ID)
        );
        Assert.assertNull(service.findByLogin("NotExistingLogin"));
        repositoryEntityManager.getTransaction().begin();
        userRepository.removeById(user.getId());
        repositoryEntityManager.getTransaction().commit();
    }

    @Test
    public void findByEmail() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.add(NEW_USER);
        repositoryEntityManager.getTransaction().commit();
        @Nullable final UserDto user = service.findByEmail(NEW_USER.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(NEW_USER, user);
        Assert.assertThrows(
                EmailEmptyException.class,
                () -> service.findByEmail(USER_EMPTY_ID)
        );
        Assert.assertNull(service.findByEmail("NotExistingEmail"));
        repositoryEntityManager.getTransaction().begin();
        userRepository.removeById(user.getId());
        repositoryEntityManager.getTransaction().commit();
    }

    @Test
    public void isLoginExist() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.add(NEW_USER);
        repositoryEntityManager.getTransaction().commit();
        Assert.assertTrue(service.isLoginExist(NEW_USER.getLogin()));
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.isLoginExist(USER_EMPTY_ID)
        );
        Assert.assertFalse(service.isLoginExist("NotExistingLogin"));
        repositoryEntityManager.getTransaction().begin();
        userRepository.removeById(NEW_USER.getId());
        repositoryEntityManager.getTransaction().commit();
    }

    @Test
    public void isEmailExist() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.add(NEW_USER);
        repositoryEntityManager.getTransaction().commit();
        Assert.assertTrue(service.isEmailExist(NEW_USER.getEmail()));
        Assert.assertThrows(
                EmailEmptyException.class,
                () -> service.isEmailExist(USER_EMPTY_ID)
        );
        Assert.assertFalse(service.isEmailExist("NotExistingEmail"));
        repositoryEntityManager.getTransaction().begin();
        userRepository.removeById(NEW_USER.getId());
        repositoryEntityManager.getTransaction().commit();
    }

}
