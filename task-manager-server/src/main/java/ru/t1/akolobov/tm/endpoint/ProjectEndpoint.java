package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.akolobov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.akolobov.tm.api.service.dto.IProjectDtoService;
import ru.t1.akolobov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;
import ru.t1.akolobov.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.akolobov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.changeStatusById(
                userId,
                request.getId(),
                request.getStatus()
        );
        return new ProjectChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.changeStatusById(
                userId,
                request.getId(),
                Status.COMPLETED
        );
        return new ProjectCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse create(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new ProjectCreateResponse(
                projectService.create(
                        userId,
                        request.getName(),
                        request.getDescription()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectGetByIdResponse getById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new ProjectGetByIdResponse(
                projectService.findOneById(userId, request.getId())
        );
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse list(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new ProjectListResponse(
                projectService.findAll(
                        userId,
                        request.getSortType()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectTaskService.removeProjectById(userId, request.getId());
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.changeStatusById(
                userId,
                request.getId(),
                Status.IN_PROGRESS
        );
        return new ProjectStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.updateById(
                userId,
                request.getId(),
                request.getName(),
                request.getDescription()
        );
        return new ProjectUpdateByIdResponse();
    }

}
