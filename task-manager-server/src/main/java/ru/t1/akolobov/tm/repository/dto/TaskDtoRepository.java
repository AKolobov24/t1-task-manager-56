package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> implements ITaskDtoRepository {

    public TaskDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull String jpql = "DELETE FROM TaskDto";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public TaskDto findOneById(@NotNull String id) {
        return entityManager.find(TaskDto.class, id);
    }

    @NotNull
    @Override
    public List<TaskDto> findAll() {
        @NotNull String jpql = "SELECT t FROM TaskDto t";
        return entityManager.createQuery(jpql, TaskDto.class)
                .getResultList();
    }

    @Override
    public @NotNull List<TaskDto> findAll(@NotNull Sort sort) {
        @NotNull String jpql = "SELECT t FROM TaskDto t ORDER BY t." + sort.getColumnName();
        return entityManager.createQuery(jpql, TaskDto.class)
                .getResultList();
    }

    @Override
    public Long getSize() {
        @NotNull String jpql = "SELECT COUNT(t) FROM TaskDto t";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        TaskDto taskDTO = entityManager.find(TaskDto.class, id);
        if (taskDTO == null) return;
        entityManager.remove(taskDTO);
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull String jpql = "DELETE FROM TaskDto t WHERE t.userId= :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@NotNull String userId) {
        @NotNull String jpql = "SELECT t FROM TaskDto t WHERE t.userId= :userId";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@NotNull String userId, @NotNull Sort sort) {
        @NotNull String jpql = "SELECT t FROM TaskDto t WHERE t.userId= :userId " +
                "ORDER BY t." + sort.getColumnName();
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDto findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull String jpql = "SELECT t FROM TaskDto t " +
                "WHERE t.id = :id AND t.userId= :userId";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Override
    public Long getSize(@NotNull String userId) {
        @NotNull String jpql = "SELECT COUNT(t) FROM TaskDto t WHERE t.userId= :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        TaskDto taskDTO = findOneById(userId, id);
        if (taskDTO == null) return;
        entityManager.remove(taskDTO);
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull String jpql = "SELECT t FROM TaskDto t " +
                "WHERE t.userId= :userId AND t.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
