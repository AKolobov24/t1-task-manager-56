package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.akolobov.tm.api.service.IAuthService;
import ru.t1.akolobov.tm.dto.model.SessionDto;
import ru.t1.akolobov.tm.dto.request.AbstractUserRequest;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.exception.user.AccessDeniedException;

@Controller
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    protected IAuthService authService;

    protected SessionDto check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        @NotNull final SessionDto session = check(request);
        if (role == null) throw new AccessDeniedException();
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    protected SessionDto check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return authService.validateToken(token);
    }

}
