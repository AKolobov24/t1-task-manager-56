package ru.t1.akolobov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.api.repository.model.IRepository;
import ru.t1.akolobov.tm.api.service.model.IService;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    protected abstract IRepository<M> getRepository();

    @Override
    public void add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        List<M> newModels;
        try {
            entityManager.getTransaction().begin();
            newModels = new ArrayList<>(repository.add(models));
            entityManager.getTransaction().commit();
            return newModels;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        List<M> newModels;
        try {
            entityManager.getTransaction().begin();
            newModels = new ArrayList<>(repository.set(models));
            entityManager.getTransaction().commit();
            return newModels;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existById(id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Long getSize() {
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
