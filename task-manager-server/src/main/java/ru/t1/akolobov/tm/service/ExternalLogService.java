package ru.t1.akolobov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.api.service.IExternalLogService;
import ru.t1.akolobov.tm.dto.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public final class ExternalLogService implements IExternalLogService {

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    private Connection connection;

    private Session session;

    private MessageProducer messageProducer;

    private boolean isDefined = true;

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @SneakyThrows
    public ExternalLogService() {
        if (connectionFactory == null) {
            isDefined = false;
            System.err.println(
                    "ERROR: ExternalLogService was not initialized due to: \n" +
                            "\t connectionFactory is null!"
            );
            return;
        }
        try {
            connection = connectionFactory.createConnection();
        } catch (Exception e) {
            isDefined = false;
            System.err.println(
                    "ERROR: ExternalLogService was not initialized due to: \n" +
                            "\t" + e.getMessage()
            );
            e.printStackTrace();
            return;
        }
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(propertyService.getExternalLoggerQueue());
        messageProducer = session.createProducer(destination);
        System.out.println("External logger was initialized!");
    }

    @SneakyThrows
    public void send(@NotNull final String text) {
        final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
    }

    @Override
    public void send(@NotNull final OperationEvent event) {
        executorService.submit(() -> sync(event));
    }

    @SneakyThrows
    public void sync(@NotNull final OperationEvent event) {
        @NotNull final Class<?> entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Table table = entityClass.getAnnotation(Table.class);
            event.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(event));
    }

    @Override
    @SneakyThrows
    public void stop() {
        executorService.shutdown();
        if (!isDefined) return;
        session.close();
        connection.close();
    }

    @Override
    public boolean isDefined() {
        return isDefined;
    }

}
