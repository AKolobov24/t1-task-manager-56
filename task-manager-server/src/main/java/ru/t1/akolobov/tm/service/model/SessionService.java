package ru.t1.akolobov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.api.repository.model.ISessionRepository;
import ru.t1.akolobov.tm.api.service.model.ISessionService;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.exception.entity.SessionNotFoundException;
import ru.t1.akolobov.tm.exception.field.DateEmptyException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.exception.user.RoleEmptyException;
import ru.t1.akolobov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.Optional;

@Service
public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    @NotNull
    @Override
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

    @NotNull
    @Override
    public Session updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date,
            @Nullable final Role role
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (date == null) throw new DateEmptyException();
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final Session session = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(SessionNotFoundException::new);
            session.setDate(date);
            session.setRole(role);
            entityManager.getTransaction().begin();
            repository.update(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
