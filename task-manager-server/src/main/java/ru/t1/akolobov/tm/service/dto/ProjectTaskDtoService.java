package ru.t1.akolobov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.akolobov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.akolobov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.akolobov.tm.exception.field.TaskIdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private ITaskDtoRepository getTaskRepository() {
        return context.getBean(ITaskDtoRepository.class);
    }

    @NotNull
    private IProjectDtoRepository getProjectRepository() {
        return context.getBean(IProjectDtoRepository.class);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            if (!projectRepository.existById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final TaskDto task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager taskEntityManager = taskRepository.getEntityManager();
        @NotNull final EntityManager projectEntityManager = projectRepository.getEntityManager();
        try {
            @NotNull final List<TaskDto> tasks = taskRepository.findAllByProjectId(userId, projectId);
            taskEntityManager.getTransaction().begin();
            tasks.forEach(t -> taskRepository.removeById(userId, t.getId()));
            projectEntityManager.getTransaction().begin();
            projectRepository.removeById(userId, projectId);
            taskEntityManager.getTransaction().commit();
            projectEntityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskEntityManager.getTransaction().rollback();
            projectEntityManager.getTransaction().rollback();
            throw e;
        } finally {
            taskEntityManager.close();
            projectEntityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(final @Nullable String userId, final @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            @NotNull final TaskDto task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
