package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.api.repository.model.ISessionRepository;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull String jpql = "DELETE FROM Session";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull String id) {
        return entityManager.find(Session.class, id);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        @NotNull String jpql = "SELECT s FROM Session s";
        return entityManager.createQuery(jpql, Session.class)
                .getResultList();
    }

    @Override
    public @NotNull List<Session> findAll(@NotNull Sort sort) {
        @NotNull String jpql = "SELECT s FROM Session s ORDER BY s." + sort.getColumnName();
        return entityManager.createQuery(jpql, Session.class)
                .getResultList();
    }

    @Override
    public Long getSize() {
        @NotNull String jpql = "SELECT COUNT(s) FROM Session s";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        Session session = entityManager.find(Session.class, id);
        if (session == null) return;
        entityManager.remove(session);
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull String jpql = "DELETE FROM Session s WHERE s.user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull String userId) {
        @NotNull String jpql = "SELECT s FROM Session s WHERE s.user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull String userId, @NotNull Sort sort) {
        @NotNull String jpql = "SELECT s FROM Session s WHERE s.user.id = :userId " +
                "ORDER BY s." + sort.getColumnName();
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull String jpql = "SELECT s FROM Session s " +
                "WHERE s.id = :id AND s.user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Override
    public Long getSize(@NotNull String userId) {
        @NotNull String jpql = "SELECT COUNT(s) FROM Session s WHERE s.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        Session session = findOneById(userId, id);
        if (session == null) return;
        entityManager.remove(session);
    }

}
