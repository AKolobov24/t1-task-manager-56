package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.akolobov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.akolobov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.akolobov.tm.api.service.dto.ITaskDtoService;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;
import ru.t1.akolobov.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.akolobov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @NotNull
    @Autowired
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectTaskService.bindTaskToProject(
                userId,
                request.getProjectId(),
                request.getTaskId()
        );
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectTaskService.unbindTaskFromProject(
                userId,
                request.getId()
        );
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskGetByProjectIdResponse getByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByProjectIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new TaskGetByProjectIdResponse(
                taskService.findAllByProjectId(
                        userId,
                        request.getProjectId()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        @NotNull final String userId = check(request).getUserId();
        taskService.changeStatusById(
                userId,
                request.getId(),
                request.getStatus()
        );
        return new TaskChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.changeStatusById(
                userId,
                request.getId(),
                Status.COMPLETED
        );
        return new TaskCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new TaskCreateResponse(
                taskService.create(
                        userId,
                        request.getName(),
                        request.getDescription()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public TaskGetByIdResponse getTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new TaskGetByIdResponse(
                taskService.findOneById(
                        userId,
                        request.getId())
        );
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new TaskListResponse(
                taskService.findAll(
                        userId,
                        request.getSortType()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.removeById(userId, request.getId());
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.changeStatusById(
                userId,
                request.getId(),
                Status.IN_PROGRESS
        );
        return new TaskStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.updateById(
                userId,
                request.getId(),
                request.getName(),
                request.getDescription()
        );
        return new TaskUpdateByIdResponse();
    }

}
