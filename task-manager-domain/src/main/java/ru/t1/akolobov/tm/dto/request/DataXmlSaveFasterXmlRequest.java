package ru.t1.akolobov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataXmlSaveFasterXmlRequest extends AbstractUserRequest {

    public DataXmlSaveFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
