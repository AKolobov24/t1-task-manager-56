package ru.t1.akolobov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.ProjectDto;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable ProjectDto project) {
        super(project);
    }

}
