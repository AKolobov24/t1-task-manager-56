package ru.t1.akolobov.tm.exception.user;

public final class LastNameEmptyException extends AbstractUserException {

    public LastNameEmptyException() {
        super("Error! Last name is empty...");
    }

}
