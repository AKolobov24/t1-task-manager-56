package ru.t1.akolobov.tm.logger.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class OperationEvent {

    @NotNull
    private String table;

}
