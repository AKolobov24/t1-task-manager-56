package ru.t1.akolobov.tm.logger.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void log(@NotNull String message);

}
