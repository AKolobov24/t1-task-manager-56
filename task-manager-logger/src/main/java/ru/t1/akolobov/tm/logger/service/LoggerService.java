package ru.t1.akolobov.tm.logger.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.logger.api.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class LoggerService implements ILoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    @Autowired
    private MongoDatabase database;

    @Override
    @SneakyThrows
    public void log(@NotNull String message) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        @NotNull final MongoCollection<Document> collection = database.getCollection(collectionName);
        collection.insertOne(new Document(event));
        System.out.println(message);
    }

}
