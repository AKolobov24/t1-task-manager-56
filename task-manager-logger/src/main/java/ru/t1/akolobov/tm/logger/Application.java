package ru.t1.akolobov.tm.logger;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.logger.component.Bootstrap;
import ru.t1.akolobov.tm.logger.configuration.LoggerConfiguration;

public class Application {

    public static void main(String[] args) throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}
