package ru.t1.akolobov.tm.logger.configuration;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.akolobov.tm.logger.api.IPropertyService;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.akolobov.tm.logger")
public class LoggerConfiguration {

    @NotNull
    @Autowired
    IPropertyService propertyService;

    @Bean
    public ConnectionFactory factory() {
        @NotNull final String url = propertyService.getConsumerUrl();
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(url);
        return factory;
    }

    @Bean
    public MongoDatabase database() {
        @NotNull MongoClient mongoClient = new MongoClient(
                propertyService.getDatabaseHost(),
                Integer.parseInt(propertyService.getDatabasePort())
        );
        return mongoClient.getDatabase(propertyService.getDatabaseName());
    }

    @Bean
    public BrokerService brokerService() throws Exception {
        BrokerService brokerService = new BrokerService();
        if (Boolean.parseBoolean(propertyService.getServerEnabled())) {
            @NotNull final String url = propertyService.getConsumerUrl();
            brokerService.addConnector(url);
            brokerService.start();
        }
        return brokerService;
    }

}
